#include<sstream>
#include<iostream>
#include<fstream>
#include<vector>
#include<string>
#include<map>
#include<math.h>
#include "social.h"
#include "Graph.h"
#include "simulator.h"
#include "history.h"
#include "memorypool.h"
#include "node.h"
#include "utils.h"

using namespace std;


/////THINGS TO BE SENT TO THIS JOB IN A FILE
//exploration constant -->explorationConst
//graphfile
//diffusionTime, tempsim.K, tempsim.numLegalActions,  tempsim.Discount


///global variables
char *graphfile;
char *inputfile;
double  exploration;
int diffusionTime;
int K;
int numLegalActions;
double Discount;
int numUncertainEdge;
int numAddedEdgeCounter;
bool weightedvoting;
HISTORY History;
string outputFile;
int numSimulations;
VNODE* Root;
map<int, vector<int> > *actionMap;
Graph *newMDP;

double MaxReward;
int maxAction;

MEMORY_POOL<SOCIAL_STATE> MemoryPool;

int UCB_N = 10000, UCB_n = 100;
bool InitialisedFastUCB;

void Prior(const STATE* state, const HISTORY& history,
    VNODE* vnode)
{
    vnode->SetChildren(0, 0);
    return;
}

VNODE* ExpandNode(const STATE* state)
{
    VNODE* vnode = VNODE::Create();
    vnode->Value.Set(0, 0);
    Prior(state, History, vnode);


    return vnode;
}


double FastUCB(int N, int n, double logN, double **UCB1)
{
    if (InitialisedFastUCB && N < UCB_N && n < UCB_n)
        return UCB1[N][n];

    if (n == 0)
        return Infinity;
    else
        return exploration * sqrt(logN / n);
}



void InitFastUCB(double exploration, double **UCB1)
{
    //cout << "Initialising fast UCB table... ";
    for (int N = 0; N < UCB_N; ++N)
        for (int n = 0; n < UCB_n; ++n)
            if (n == 0)
                UCB1[N][n] = Infinity;
            else
                UCB1[N][n] = exploration * sqrt(log(N + 1) / n);
    //cout << "done" << endl;
    InitialisedFastUCB = true;
}

int GreedyUCB(VNODE* vnode, bool ucb, SIMULATOR *sim, HISTORY *his, double **UCB1)
{
    vector<int> besta;
    besta.clear();
    double bestq = -Infinity;
    int N = vnode->Value.GetCount();
    double logN = log(N + 1);
    bool hasalpha = sim->HasAlpha();
    //cout<<"Number of actions: "<<Simulator.GetNumActions()<<"\n";

    for (int action = 0; action < sim->GetNumActions(); action++)
    {
      //cout<<"Value of action: "<<action<<"\n";
        double q, alphaq;
        int n, alphan;

        VNODE* qnode = vnode->Child(action);
        q = qnode->Value.GetValue();
        n = qnode->Value.GetCount();

        /*if (Params.UseRave && qnode.AMAF.GetCount() > 0)
        {
            double n2 = qnode.AMAF.GetCount();
            double beta = n2 / (n + n2 + Params.RaveConstant * n * n2);
            q = (1.0 - beta) * q + beta * qnode.AMAF.GetValue();
        }*/

        if (hasalpha && n > 0)
        {
            sim->AlphaValue(qnode, alphaq, alphan);
            q = (n * q + alphan * alphaq) / (n + alphan);
            //cout << "N = " << n << ", alphaN = " << alphan << endl;
            //cout << "Q = " << q << ", alphaQ = " << alphaq << endl;
        }

        if (ucb)
            q += FastUCB(N, n, logN, UCB1);

        if (q >= bestq)
        {
            if (q > bestq)
                besta.clear();
            bestq = q;
            besta.push_back(action);
            }
        }
        //cout<<"besta size: "<<besta.size()<<"\n";
        //fflush(NULL);
		assert(!besta.empty());
        return besta[rand()%besta.size()];
        //return besta[Random(besta.size())];
}

double SimulateAfterOneLevel(STATE& state, VNODE* vnode, SIMULATOR *sim, HISTORY *his)
{
    double reward=0;
    int numSteps=0;
    double currReward=0;
    bool terminal=false;
    int action;
    while(!terminal)
    {
        vector<int> tempLegalActions;
        sim->GenerateLegal(state, *his, tempLegalActions);
        //action = GreedyUCB(state, true);
        action = tempLegalActions[rand()%(tempLegalActions.size())];
        terminal = sim->Step(state, action, currReward);
        his->Add(action);
        reward+=currReward;
    }

    return reward;

}


double SimulateQ(STATE& state, VNODE* qnode, int action, SIMULATOR *sim, HISTORY *his)
{
    int observation;
    double immediateReward, delayedReward = 0;

    if (sim->HasAlpha())
        sim->UpdateAlpha(qnode, state);
    bool terminal = sim->Step(state, action, immediateReward);
    his->Add(action);



    SOCIAL_STATE *currSt = new SOCIAL_STATE();
    SOCIAL_STATE& tempcState = static_cast<SOCIAL_STATE&>(state);

    for (int i=0;i<tempcState.netVector.size();i++)
        currSt->netVector.push_back(tempcState.netVector[i]);

    VNODE* vnode = qnode;

    if (!terminal)
    {
        if (vnode)
          {
            vnode->Beliefs().AddSample(currSt);
            delayedReward = SimulateAfterOneLevel(state, vnode, sim, his);
          }
    }

    double totalReward = immediateReward + sim->GetDiscount() * delayedReward;
    qnode->Value.Add(totalReward);
    return totalReward;
}



double SimulateV(STATE& state, VNODE* vnode, SIMULATOR *sim, HISTORY *his, map<int, pair<int, double> > *maxActionMap, double **UCB1)
{
    int action = GreedyUCB(vnode, true, sim, his, UCB1);


    VNODE* qnode = vnode->Child(action);
    double totalReward = SimulateQ(state, qnode, action, sim, his);

    if (maxActionMap->find(action)!=maxActionMap->end())
    {
        maxActionMap->find(action)->second.first += 1;
        maxActionMap->find(action)->second.second += totalReward;
    }
    else
        maxActionMap->insert(pair<int, pair<int, double> >(action, pair<int, double>(1, totalReward)));

    vnode->Value.Add(totalReward);

    return totalReward;
}


void UCTSearch(SIMULATOR *sim, HISTORY *his, map<int, pair<int, double> > *maxActionMap, double **UCB1)
{
     int historyDepth = his->Size();

     for (int n=0;n<numSimulations;n++)
     {
         STATE* state = Root->Beliefs().CreateSample(*sim);
         sim->Validate(*state);
        //Status.Phase = SIMULATOR::STATUS::TREE;

         double totalReward = SimulateV(*state, Root, sim, his, maxActionMap, UCB1);

        sim->FreeState(state);
         his->Truncate(historyDepth);
     }
}




void ThreadSimulator(void *b)
{
	 //graph that is sent here is already certain so no need to pass in false to create sampled graph
	 //Graph *newMDP = new Graph(graphfile, diffusionTime, true, 0.5, 0.5);
	
	 ///createActionMap
	
    double **UCB1;

    UCB1 = new double*[UCB_N];

   for (int i=0;i<UCB_N;i++)
   {
    UCB1[i] = new double[UCB_n];
   }
   

    InitFastUCB(exploration, UCB1);

    /////since actionMap is never changed...it can be shared between threads
    map<int, pair<int, double> > *maxActionMap = new map<int, pair<int, double> >();

    //SOCIAL& tempsim = dynamic_cast<SOCIAL&>(Simulator);

    //Graph *newMDP = new Graph(graphfile, diffusionTime, false, tempsim.graph->existenceProb, tempsim.graph->propagationProb);

    SOCIAL *sim = new SOCIAL(graphfile, diffusionTime, K, numLegalActions, newMDP, actionMap, Discount, true);

    int maxVote=1;

    int addedVal=0;
    if (weightedvoting)
    {
        int val = (newMDP->UncertainEdge)/2;
        maxVote = val;
        if ((newMDP->UncertainEdge)%2==0)
            maxVote = maxVote - abs(val - newMDP->addedEdgeCounter);
        else
        {
            if (newMDP->addedEdgeCounter!=val||newMDP->addedEdgeCounter!=val+1)
            {
                if (newMDP->addedEdgeCounter>val+1)
                    maxVote = maxVote -  abs(newMDP->addedEdgeCounter - val - 1);
                else
                    maxVote = maxVote - abs(val - newMDP->addedEdgeCounter);
            }
        }
    }
   

     HISTORY *his = new HISTORY();
     for (int i=0;i<History.Size();i++)
        his->Add(History[i].Action);


    UCTSearch(sim, his, maxActionMap, UCB1);

    MaxReward = -10000;
    maxAction=-1;

    for (map<int, pair<int, double> >::iterator it = maxActionMap->begin();it!=maxActionMap->end();it++)
    {
        double temp= (it->second.second)/(it->second.first);
        if (temp>MaxReward)
        {
            MaxReward = temp;
            maxAction = it->first;
        }
    }

	

     /////write answer to file....voting will be done via shell script
	string fil("voting");
	ofstream myfile;
  	myfile.open (fil.append(outputFile));
  	myfile <<maxAction<<" "<<maxVote<<"\n";

	myfile.close();
     /*if (finalActionMap.find(maxAction)!=finalActionMap.end())
    {
          finalActionMap.find(maxAction)->second.first += maxVote;

        finalActionMap.find(maxAction)->second.second += 1;  ///dummy...does not do anything
    }
    else
        finalActionMap.insert(pair<int, pair<int, double> >(maxAction, pair<int, double>(maxVote, 1)));
	*/
    ////critical section ends

    delete newMDP;
    delete his;
    delete maxActionMap;

   for (int i=0;i<UCB_N;i++)
   {
      delete UCB1[i];
   }
}

STATE* CreateStartState()
{
    SOCIAL_STATE* bsstate = MemoryPool.Allocate();

    /////initially all nodes are uninfluenced
    for (int i=0;i<newMDP->numNodes;i++)
        bsstate->netVector.push_back(0);

    return bsstate;
}


int main(int argc, char *argv[])
{
	if (argc!=4)
    {
        cout<<"Wrong number of arguments\n";
        cout<<"Usage: ./a.out <graphfileName> <otherInputFile> <belief_file>\n";
        return 0;
    }

	weightedvoting=false;
	graphfile = argv[1];

    ///contains various command line parameters
    inputfile = argv[2];


    ////read in parameters and populate history
    ifstream myReadFile;
    myReadFile.open(argv[2]);

	int maxDouble;

    if (myReadFile.is_open())
    {
        myReadFile>>exploration>>diffusionTime>>K>>numLegalActions>>Discount>>numUncertainEdge>>numAddedEdgeCounter>>outputFile>>maxDouble;
		int temp;
		
        while (myReadFile>>temp) {
            History.Add(temp);
        }
    }
    myReadFile.close();
    //////end reading file

	

	
	newMDP = new Graph(graphfile, diffusionTime, true, 0.5, 0.5);

	////createActionMap
	actionMap = new map<int, vector<int> >();

    int actionIndex=0;
    vector<int> tempvec;

    for (int i=0;i<newMDP->numNodes;i++)
    {
        if (i>newMDP->numNodes-1-K)
            tempvec.push_back(1);
        else
            tempvec.push_back(0);
    }

    bool end=false;

    while(!end)
    {
        vector<int> *newvec = new vector<int>();
        for (int i=0;i<tempvec.size();i++)
            newvec->push_back(tempvec[i]);
        actionMap->insert(pair<int, vector<int> >(actionIndex++, *newvec));


        ////generate next binary string with k bits set
        bool nextPresent=false;
        for (int i=tempvec.size()-1;i>=1;i--)
        {
            if (tempvec[i]==1&&tempvec[i-1]==0)
            {
                nextPresent=true;
                tempvec[i]=0;
                tempvec[i-1]=1;
                int numOnesAfterPoint = 0;
                for (int j=i+1;j<tempvec.size();j++)
                    if (tempvec[j]==1)
                        numOnesAfterPoint++;

                for (int j=tempvec.size()-1;j>=i+1;j--)
                {
                    if (numOnesAfterPoint)
                    {
                        tempvec[j]=1;
                        numOnesAfterPoint--;
                    }
                    else
                        tempvec[j]=0;
                }
                break;
			  }
            else
                continue;
        }

        if (!nextPresent)
            end=true;

    }
    ////actionMap created


/*	for (map<int, vector<int> >::iterator it=actionMap->begin();it!=actionMap->end();it++)
	{
		vector<int> tmp = it->second;
		cout<<it->first<<": ";
		for (int j=0;j<tmp.size();j++)
			cout<<tmp[j]<<",";
		cout<<"\n";
		
	}*/

	

	VNODE::NumChildren = actionMap->size();;
	
	////create Root node
	numSimulations = 1<<maxDouble;

	Root = ExpandNode(CreateStartState());
	
	ifstream beliefFile;
    beliefFile.open(argv[3]);

	string line;
	if (beliefFile.is_open())
    {
		while(getline(beliefFile, line))
		{
			SOCIAL_STATE *tempState = static_cast<SOCIAL_STATE *>(CreateStartState());
			istringstream iss(line);
			 for (int i=0;i<newMDP->numNodes;i++)
                iss>>tempState->netVector[i];
			Root->Beliefs().AddSample(tempState);
		}
		
    }


	beliefFile.close();

	

	///end reading file

	ThreadSimulator(NULL);	

}

