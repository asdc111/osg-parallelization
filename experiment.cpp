#include "boost/timer.hpp"
#include "experiment.h"
#include "social.h"
#include "Graph.h"

using namespace std;


EXPERIMENT::PARAMS::PARAMS()
:   NumRuns(1000),
    NumSteps(100000),
    SimSteps(1000),
    TimeOut(3600),
    MinDoubles(0),
    MaxDoubles(20),
    TransformDoubles(-4),
    TransformAttempts(1000),
    Accuracy(0.01),
    UndiscountedHorizon(1000),
    AutoExploration(true)
{
}

EXPERIMENT::EXPERIMENT(SIMULATOR& real,
    SIMULATOR& simulator, const string& outputFile,
    EXPERIMENT::PARAMS& expParams, QMDP::PARAMS& searchParams, int number, bool weighted, string graphe, int timeer, bool option)
:   Real(real),
    Simulator(simulator),
    OutputFile(outputFile.c_str()),
    ExpParams(expParams),
    SearchParams(searchParams)
{
	 option2 = option;
	 graphfile = graphe;
	 diffusionTime = timeer;
	 numberMDP = number;
	 weightedvoting = weighted;
    if (ExpParams.AutoExploration)
    {
        if (SearchParams.UseRave)
            SearchParams.ExplorationConstant = 0;
        else
            SearchParams.ExplorationConstant = simulator.GetRewardRange();
    }
    QMDP::InitFastUCB(SearchParams.ExplorationConstant);
}


void EXPERIMENT::Run()
{
    boost::timer timer;

    QMDP qmdp(Simulator, SearchParams, numberMDP, weightedvoting, graphfile, diffusionTime, option2, SearchParams.ExplorationConstant);
	
    double undiscountedReturn = 0.0;
	double quadraticreward = 0.0;
    double discountedReturn = 0.0;
    double discount = 1.0;
    bool terminal = false;
	bool outOfParticles = false;
    int t;

    STATE* state = Real.CreateStartState();

    for (t = 0; t < ExpParams.NumSteps; t++)
    {
        int observation;
        double reward;
        int action = qmdp.SelectAction();
		  //////////////////////////////code for printing stuff
	
		  vector<int> actionVec;
		  actionVec.clear();
		  SOCIAL& sim = dynamic_cast<SOCIAL&>(Real);
		  actionVec = sim.actionMap->find(action)->second;
		
		  cout<<"Action for this round:\n";
		 
		  vector<int> currAction;
		  for (int s=0;s<actionVec.size();s++)
		  {
			if (actionVec[s]==1)
			{
				currAction.push_back(s);
			//	cout<<s<<",";
			}
		  }
			//cout<<"\n";
			
		  //cout<<"Action of this round:\n";
         for (int b=0;b<currAction.size();b++)
         {
            int index=currAction[b];
            int count=0;
            for (map<int, map<int, pair<int, int> > >::iterator it=sim.graph->adjList.begin();it!=sim.graph->adjList.end();it++,count++)
            {
               if (count==index)
               {
                  cout<<it->first<<",";
                  break;
               }
            }
         }  
         cout<<"\n";
			////////////////////////////code for printing stuff ends

        terminal = Real.Step(*state, action,reward);
		Real.DisplayState(*state, cout);
		 
        cout<<"Reward for this round: "<<reward<<"\n";

        Results.Reward.Add(reward);
		quadraticreward +=(undiscountedReturn+reward);
        undiscountedReturn += reward;
        discountedReturn += reward * discount;
        discount *= Real.GetDiscount();

        if (SearchParams.Verbose >= 1)
        {
            Real.DisplayAction(action, cout);
            Real.DisplayState(*state, cout);
            Real.DisplayObservation(*state, observation, cout);
            Real.DisplayReward(reward, cout);
        }

        if (terminal)
        {
            cout << "Terminated" << endl;
            break;
        }
        outOfParticles = !qmdp.Update(action, reward);
        if (outOfParticles)
            break;

    }


    if (outOfParticles)
    {
        cout << "Out of particles, finishing episode with SelectRandom" << endl;
        HISTORY history = qmdp.GetHistory();
        while (++t < ExpParams.NumSteps)
        {
            int observation;
            double reward;

            // This passes real state into simulator!
            // SelectRandom must only use fully observable state
            // to avoid "cheating"
            int action = Simulator.SelectRandom(*state, history, qmdp.GetStatus());
            terminal = Real.Step(*state, action,  reward);

            Results.Reward.Add(reward);
			quadraticreward +=(undiscountedReturn+reward);
            undiscountedReturn += reward;
            discountedReturn += reward * discount;
            discount *= Real.GetDiscount();

            if (SearchParams.Verbose >= 1)
            {
                Real.DisplayAction(action, cout);
                Real.DisplayState(*state, cout);
                Real.DisplayObservation(*state, observation, cout);
                Real.DisplayReward(reward, cout);
            }

            if (terminal)
            {
                cout << "Terminated" << endl;
                break;
            }

            history.Add(action);
        }
    }

    Results.Time.Add(timer.elapsed());
	Results.UndiscountedReturn.Add(undiscountedReturn);
    Results.DiscountedReturn.Add(discountedReturn);
	Results.QuadraticReturn.Add(quadraticreward);
	Results.LinearReturnDividedT.Add(undiscountedReturn/t);
    cout << "Discounted return = " << discountedReturn
        << ", average = " << Results.DiscountedReturn.GetMean() << endl;
    cout << "Undiscounted return = " << undiscountedReturn
        << ", average = " << Results.UndiscountedReturn.GetMean() << endl;
	 cout << "Quadratic return = " << quadraticreward
        << ", average = " << Results.QuadraticReturn.GetMean() << endl;
    cout << "Linear return divided by steps = " << (undiscountedReturn/t)
        << ", average = " << Results.LinearReturnDividedT.GetMean() << endl;
}

void EXPERIMENT::MultiRun()
{
    for (int n = 0; n < ExpParams.NumRuns; n++)
    {
        cout << "Starting run " << n + 1 << " with "
            << SearchParams.NumSimulations << " simulations... " << endl;
        Run();
    }
}

void EXPERIMENT::DiscountedReturn()
{
    cout << "Main runs" << endl;
    OutputFile << "Simulations\tRuns\tUndiscounted return\tUndiscounted error\tDiscounted return\tDiscounted error\tQuadratic return\tQuadratic error\tRewards divided\tError\tTime\n";

    SearchParams.MaxDepth = Simulator.GetHorizon(ExpParams.Accuracy, ExpParams.UndiscountedHorizon);
    ExpParams.SimSteps = Simulator.GetHorizon(ExpParams.Accuracy, ExpParams.UndiscountedHorizon);
    ExpParams.NumSteps = Real.GetHorizon(ExpParams.Accuracy, ExpParams.UndiscountedHorizon);

    for (int i = ExpParams.MinDoubles; i <= ExpParams.MaxDoubles; i++)
    {
        SearchParams.NumSimulations = 1 << i;
        SearchParams.NumStartStates = 1 << i;
        if (i + ExpParams.TransformDoubles >= 0)
            SearchParams.NumTransforms = 1 << (i + ExpParams.TransformDoubles);
        else
            SearchParams.NumTransforms = 1;
        SearchParams.MaxAttempts = SearchParams.NumTransforms * ExpParams.TransformAttempts;

        Results.Clear();
        MultiRun();

        cout << "Simulations = " << SearchParams.NumSimulations << endl
            << "Runs = " << Results.Time.GetCount() << endl
            << "Undiscounted return = " << Results.UndiscountedReturn.GetMean()
            << " +- " << Results.UndiscountedReturn.GetStdErr() << endl
            << "Discounted return = " << Results.DiscountedReturn.GetMean()
            << " +- " << Results.DiscountedReturn.GetStdErr() << endl
            << "Time = " << Results.Time.GetMean() << endl;
 OutputFile << SearchParams.NumSimulations << "\t"
            << Results.Time.GetCount() << "\t"
            << Results.UndiscountedReturn.GetMean() << "\t"
            << Results.UndiscountedReturn.GetStdErr() << "\t"
            << Results.DiscountedReturn.GetMean() << "\t"
            << Results.DiscountedReturn.GetStdErr() << "\t"
				<< Results.QuadraticReturn.GetMean() << "\t"
            << Results.QuadraticReturn.GetStdErr() << "\t"
            << Results.LinearReturnDividedT.GetMean() << "\t"
            << Results.LinearReturnDividedT.GetStdErr() << "\t"
            << Results.Time.GetMean() << endl;
    }
}


void EXPERIMENT::AverageReward()
{
    cout << "Main runs" << endl;
    OutputFile << "Simulations\tSteps\tAverage reward\tAverage time\n";

    SearchParams.MaxDepth = Simulator.GetHorizon(ExpParams.Accuracy, ExpParams.UndiscountedHorizon);
    ExpParams.SimSteps = Simulator.GetHorizon(ExpParams.Accuracy, ExpParams.UndiscountedHorizon);

    for (int i = ExpParams.MinDoubles; i <= ExpParams.MaxDoubles; i++)
    {
        SearchParams.NumSimulations = 1 << i;
        SearchParams.NumStartStates = 1 << i;
        if (i + ExpParams.TransformDoubles >= 0)
            SearchParams.NumTransforms = 1 << (i + ExpParams.TransformDoubles);
        else
            SearchParams.NumTransforms = 1;
        SearchParams.MaxAttempts = SearchParams.NumTransforms * ExpParams.TransformAttempts;

        Results.Clear();
        Run();

        cout << "Simulations = " << SearchParams.NumSimulations << endl
            << "Steps = " << Results.Reward.GetCount() << endl
            << "Average reward = " << Results.Reward.GetMean()
            << " +- " << Results.Reward.GetStdErr() << endl
            << "Average time = " << Results.Time.GetMean() / Results.Reward.GetCount() << endl;
        OutputFile << SearchParams.NumSimulations << "\t"
            << Results.Reward.GetCount() << "\t"
            << Results.Reward.GetMean() << "\t"
            << Results.Reward.GetStdErr() << "\t"
            << Results.Time.GetMean() / Results.Reward.GetCount() << endl;
    }
}


