#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "qmdp.h"
#include "simulator.h"
#include "statistic.h"
#include <fstream>
#include<string>

//----------------------------------------------------------------------------

struct RESULTS
{
    void Clear();

    STATISTIC Time;
    STATISTIC Reward;
    STATISTIC DiscountedReturn;
    STATISTIC UndiscountedReturn;
	 STATISTIC QuadraticReturn;
	 STATISTIC LinearReturnDividedT;
};

inline void RESULTS::Clear()
{
    Time.Clear();
    Reward.Clear();
    DiscountedReturn.Clear();
    UndiscountedReturn.Clear();
	 QuadraticReturn.Clear();
	 LinearReturnDividedT.Clear();
}

//----------------------------------------------------------------------------

class EXPERIMENT
{
public:

    struct PARAMS
    {
        PARAMS();
        
        int NumRuns;
        int NumSteps;
        int SimSteps;
        double TimeOut;
        int MinDoubles, MaxDoubles;
        int TransformDoubles;
        int TransformAttempts;
        double Accuracy;
        int UndiscountedHorizon;
        bool AutoExploration;
    };

    EXPERIMENT(SIMULATOR& real, SIMULATOR& simulator, 
        const std::string& outputFile, 
        EXPERIMENT::PARAMS& expParams, QMDP::PARAMS& searchParams, int numberMDP, bool weightedvoting, string graphfile, int diffusionTime, bool option2);

    void Run();
    void MultiRun();
    void DiscountedReturn();
    void AverageReward();
	 int numberMDP;
	 bool weightedvoting;
	 string graphfile;
	 int diffusionTime;
	 bool option2;


    SIMULATOR& Real;
    SIMULATOR& Simulator;
	 private:
    EXPERIMENT::PARAMS& ExpParams;
    QMDP::PARAMS& SearchParams;
    RESULTS Results;

    std::ofstream OutputFile;
};

//----------------------------------------------------------------------------

#endif // EXPERIMENT_H
