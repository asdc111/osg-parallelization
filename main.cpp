#include "qmdp.h"
#include "social.h"
#include "experiment.h"
#include <boost/program_options.hpp>

using namespace std;
using namespace boost::program_options;


int main(int argc, char* argv[])
{
    QMDP::PARAMS searchParams;
    EXPERIMENT::PARAMS expParams;
    SIMULATOR::KNOWLEDGE knowledge;
    string problem, outputfile, policy;
    int size, number, treeknowledge = 1, rolloutknowledge = 1, smarttreecount = 10;
    double smarttreevalue = 1.0;
	bool weightedvoting, option2;
	int numberMDP;
	double discountVal;
    double existenceProb, propagationProb;
	 
	
	string graphfile;
	int diffusionTime, numResources, numLegalActions;

    options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("test", "run unit tests")
        ("problem", value<string>(&problem), "problem to run")
	     	("graphfile", value<string>(&graphfile), "graph input file")
		  	("option2", value<bool>(&option2)->default_value(false), "second option for voting")
		  	("weightedvoting", value<bool>(&weightedvoting)->default_value(false), "true if weighted voting is used, false if not used")
		  	("numberMDPs", value<int>(&numberMDP), "number of sampled MDPs")
	     	("diffusionTime", value<int>(&diffusionTime), "tunable parameter for influence spread")
		  	("numResources", value<int>(&numResources), "number of nodes picked per round")
		  	("numLegalActions", value<int>(&numLegalActions), "number of legalActions returned per LegalAction call")
        ("outputfile", value<string>(&outputfile)->default_value("output.txt"), "summary output file")
        ("policy", value<string>(&policy), "policy file (explicit POMDPs only)")
        ("size", value<int>(&size), "size of problem (problem specific)")
        ("number", value<int>(&number), "number of elements in problem (problem specific)")
        ("timeout", value<double>(&expParams.TimeOut), "timeout (seconds)")
        ("mindoubles", value<int>(&expParams.MinDoubles), "minimum power of two simulations")
        ("maxdoubles", value<int>(&expParams.MaxDoubles), "maximum power of two simulations")
        ("runs", value<int>(&expParams.NumRuns), "number of runs")
        ("accuracy", value<double>(&expParams.Accuracy), "accuracy level used to determine horizon")
        ("horizon", value<int>(&expParams.UndiscountedHorizon), "horizon to use when not discounting")
		("discount", value<double>(&discountVal), "discount value to be used")
        ("num steps", value<int>(&expParams.NumSteps), "number of steps to run when using average reward")
        ("verbose", value<int>(&searchParams.Verbose), "verbosity level")
        ("autoexploration", value<bool>(&expParams.AutoExploration), "Automatically assign UCB exploration constant")
        ("exploration", value<double>(&searchParams.ExplorationConstant), "Manual value for UCB exploration constant")
        ("usetransforms", value<bool>(&searchParams.UseTransforms), "Use transforms")
        ("transformdoubles", value<int>(&expParams.TransformDoubles), "Relative power of two for transforms compared to simulations")
        ("transformattempts", value<int>(&expParams.TransformAttempts), "Number of attempts for each transform")
        ("userave", value<bool>(&searchParams.UseRave), "RAVE")
        ("ravediscount", value<double>(&searchParams.RaveDiscount), "RAVE discount factor")
        ("raveconstant", value<double>(&searchParams.RaveConstant), "RAVE bias constant")
        ("treeknowledge", value<int>(&knowledge.TreeLevel), "Knowledge level in tree (0=Pure, 1=Legal, 2=Smart)")
        ("rolloutknowledge", value<int>(&knowledge.RolloutLevel), "Knowledge level in rollouts (0=Pure, 1=Legal, 2=Smart)")
        ("smarttreecount", value<int>(&knowledge.SmartTreeCount), "Prior count for preferred actions during smart tree search")
        ("smarttreevalue", value<double>(&knowledge.SmartTreeValue), "Prior value for preferred actions during smart tree search")
        ("disabletree", value<bool>(&searchParams.DisableTree), "Use 1-ply rollout action selection")
			("existenceProb", value<double>(&existenceProb), "Existence probability of uncertain edges")
			("propagationProb", value<double>(&propagationProb), "Propagation probability on edges")
        ;

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
    {
        cout << desc << "\n";
        return 1;
    }

    if (vm.count("problem") == 0)
    {
        cout << "No problem specified" << endl;
        return 1;
    }

    if (vm.count("test"))
    {
        cout << "Running unit tests" << endl;
        //UnitTests();
        return 0;
    }

    SIMULATOR* real = 0;
    SIMULATOR* simulator = 0;

	if (problem == "social")
	{
	Graph *graph1 = new Graph(graphfile, diffusionTime, true, existenceProb, propagationProb); ////real graph
	Graph *graph2 = new Graph(graphfile, diffusionTime, false, existenceProb, propagationProb); /////sample MDP
	map<int, vector<int> > *actionMap = new map<int, vector<int> >();

	int actionIndex=0;
    vector<int> tempvec;
    
    for (int i=0;i<graph1->numNodes;i++)
    {
        if (i>graph1->numNodes-1-numResources)
            tempvec.push_back(1);
        else
            tempvec.push_back(0);
    }
    
    bool end=false;
    
    while(!end)
    {
        vector<int> *newvec = new vector<int>();
        for (int i=0;i<tempvec.size();i++)
            newvec->push_back(tempvec[i]);
        actionMap->insert(pair<int, vector<int> >(actionIndex++, *newvec));
        
        
        ////generate next binary string with k bits set
        bool nextPresent=false;
        for (int i=tempvec.size()-1;i>=1;i--)
        {
            if (tempvec[i]==1&&tempvec[i-1]==0)
            {
                nextPresent=true;
                tempvec[i]=0;
                tempvec[i-1]=1;
                int numOnesAfterPoint = 0;
                for (int j=i+1;j<tempvec.size();j++)
                    if (tempvec[j]==1)
                        numOnesAfterPoint++;
                
                for (int j=tempvec.size()-1;j>=i+1;j--)
                {
                    if (numOnesAfterPoint)
                    {
                        tempvec[j]=1;
                        numOnesAfterPoint--;
                    }
                    else
                        tempvec[j]=0;
                }
                break;
            }
            else
                continue;
        }
        
        if (!nextPresent)
            end=true;
        
    }
	////actionMap created
	

	
		  real = new SOCIAL(graphfile, diffusionTime, numResources, numLegalActions, graph1, actionMap, discountVal, false);
		  simulator = new SOCIAL(graphfile, diffusionTime, numResources, numLegalActions, graph2, actionMap, discountVal, true);
	}
    else 
    {
        cout << "Unknown problem" << endl;
        exit(1);
    }


    simulator->SetKnowledge(knowledge);
    EXPERIMENT experiment(*real, *simulator, outputfile, expParams, searchParams, numberMDP, weightedvoting, graphfile, diffusionTime, option2);
    experiment.DiscountedReturn();

    delete real;
    delete simulator;
    return 0;
}
