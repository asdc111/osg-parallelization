#include "qmdp.h"
#include <math.h>
#include "social.h"
#include <algorithm>

using namespace std;
using namespace UTILS;

QMDP::PARAMS::PARAMS()
:   Verbose(0),
    MaxDepth(100),
    NumSimulations(1000),
    NumStartStates(1000),
    UseTransforms(true),
    NumTransforms(0),
    MaxAttempts(0),
    ExpandCount(1),
    ExplorationConstant(1),
    UseRave(false),
    RaveDiscount(1.0),
    RaveConstant(0.01),
    DisableTree(false)
{
}

QMDP::QMDP(SIMULATOR& simulator, const PARAMS& params, int number, bool weighted, string graphe, int timeer, bool option, double exploration)
:   Simulator(simulator),
    Params(params),
    TreeDepth(0)
{
	 explorationConst = exploration;
	 option2 = option;
	 graphfile = graphe;
	 diffusionTime = timeer;
	 numberMDP = number;
	 weightedvoting = weighted;
	 MaxReward = -10000;
	 maxAction = -1;
     VNODE::NumChildren = Simulator.GetNumActions();

    Root = ExpandNode(Simulator.CreateStartState());

    for (int i = 0; i < Params.NumStartStates; i++)
        Root->Beliefs().AddSample(Simulator.CreateStartState());
}

QMDP::~QMDP()
{
    VNODE::Free(Root, Simulator);
    VNODE::FreeAll();
}

void *QMDP::ThreadSimulator(void *b)
{
	double **UCB1;

	UCB1 = new double*[UCB_N];

   for (int i=0;i<UCB_N;i++)
   {
   	UCB1[i] = new double[UCB_n];
   }
	

	InitFastUCB(explorationConst, UCB1);

	/////since actionMap is never changed...it can be shared between threads
	map<int, pair<int, double> > *maxActionMap = new map<int, pair<int, double> >();

	SOCIAL& tempsim = dynamic_cast<SOCIAL&>(Simulator);

	///critical section
	pthread_mutex_lock (&mutex);
	Graph *newMDP = new Graph(graphfile, diffusionTime, false, tempsim.graph->existenceProb, tempsim.graph->propagationProb);
	pthread_mutex_unlock (&mutex);
	////critical section ends
	
	//SOCIAL& tempsim = dynamic_cast<SOCIAL&>(Simulator);
	

	SOCIAL *sim = new SOCIAL(graphfile, diffusionTime, tempsim.K, tempsim.numLegalActions, newMDP, tempsim.actionMap, tempsim.Discount, true);
		
    int maxVote=1;
    
    int addedVal=0;
    if (weightedvoting)
    {
        int val = (newMDP->UncertainEdge)/2;
        maxVote = val;
        if ((newMDP->UncertainEdge)%2==0)
            maxVote = maxVote - abs(val - newMDP->addedEdgeCounter);
        else
        {
            if (newMDP->addedEdgeCounter!=val||newMDP->addedEdgeCounter!=val+1)
            {
                if (newMDP->addedEdgeCounter>val+1)
                    maxVote = maxVote -  abs(newMDP->addedEdgeCounter - val - 1);
                else
                    maxVote = maxVote - abs(val - newMDP->addedEdgeCounter);
            }
        }
    }
    
   
	 HISTORY *his = new HISTORY();
	 for (int i=0;i<History.Size();i++)
		his->Add(History[i].Action);

    UCTSearch(sim, his, maxActionMap, UCB1);

    MaxReward = -10000;
    maxAction=-1;
    
    for (map<int, pair<int, double> >::iterator it = maxActionMap->begin();it!=maxActionMap->end();it++)
    {
        double temp= (it->second.second)/(it->second.first);
        if (temp>MaxReward)
        {
            MaxReward = temp;
            maxAction = it->first;
        }
    }
		
	 /////another critical section
	 pthread_mutex_lock (&mutex);
	 if (finalActionMap.find(maxAction)!=finalActionMap.end())
    {
    	  finalActionMap.find(maxAction)->second.first += maxVote;

        finalActionMap.find(maxAction)->second.second += 1;  ///dummy...does not do anything
    }
    else
        finalActionMap.insert(pair<int, pair<int, double> >(maxAction, pair<int, double>(maxVote, 1)));

	pthread_mutex_unlock (&mutex);
	////critical section ends

	delete newMDP;	
	delete sim;
	delete his;
	delete maxActionMap;

   for (int i=0;i<UCB_N;i++)
   {
      delete UCB1[i];
   }
}

void *ThreadCaller(void *a)
{
	QMDP *qm = (QMDP *)a;
	qm->ThreadSimulator(NULL);
	pthread_exit(NULL);
}


int QMDP::SelectAction()
{
	MaxReward=-10000;
	maxAction=-1;
	finalActionMap.clear();
	
	pthread_attr_t attr;
	pthread_mutex_init(&mutex, NULL);

	pthread_attr_init(&attr);
   	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);


	for (int i=0;i<numberMDP;i++)
	{
		pthread_create(&callThd[i], &attr, ThreadCaller, this);
	}

	pthread_attr_destroy(&attr);


	/////wait for all of them to finish

	void *status;

	for(int i=0; i<numberMDP; i++)
   	{
	  pthread_join(callThd[i], &status);
	}
		
	pthread_mutex_destroy(&mutex);

	int maxVoter=-1;
	maxAction = -1;
	for (map<int, pair<int, double> >::iterator it = finalActionMap.begin();it!=finalActionMap.end();it++)
	{
		if (it->second.first>maxVoter)
		{
			maxVoter = it->second.first;
			maxAction = it->first;
		}
	}

	return maxAction;
}



bool QMDP::Update(int action,  double reward)
{
    History.Add(action);
    BELIEF_STATE beliefs;

    // Find matching vnode from the rest of the tree
    VNODE* vnode = Root->Child(action);
   
    if (vnode)
    {
        if (Params.Verbose >= 1)
            cout << "Matched " << vnode->Beliefs().GetNumSamples() << " states" << endl;
        beliefs.Copy(vnode->Beliefs(), Simulator);
    }
    else
    {
        if (Params.Verbose >= 1)
            cout << "No matching node found" << endl;
    }

    // Generate transformed states to avoid particle deprivation
    if (Params.UseTransforms)
        AddTransforms(Root, beliefs);

    // If we still have no particles, fail
    if (beliefs.Empty() && (!vnode || vnode->Beliefs().Empty()))
        return false;

    if (Params.Verbose >= 1)
        Simulator.DisplayBeliefs(beliefs, cout);

    // Find a state to initialise prior (only requires fully observed state)
    const STATE* state = 0;
    if (vnode && !vnode->Beliefs().Empty())
        state = vnode->Beliefs().GetSample(0);
    else
        state = beliefs.GetSample(0);

    VNODE::Free(Root, Simulator);
    VNODE* newRoot = ExpandNode(state);
    newRoot->Beliefs() = beliefs;
    Root = newRoot;
    return true;
}

VNODE* QMDP::ExpandNode(const STATE* state)
{
    VNODE* vnode = VNODE::Create();
    vnode->Value.Set(0, 0);
    Simulator.Prior(state, History, vnode, Status);

    if (Params.Verbose >= 2)
    {
        cout << "Expanding node: ";
        History.Display(cout);
        cout << endl;
    }

    return vnode;
}

void QMDP::AddSample(VNODE* node, const STATE& state)
{
    STATE* sample = Simulator.Copy(state);
	 //cout<<"CHAMBA\n";
    node->Beliefs().AddSample(sample);
	 //cout<<"Rani\n";
    if (Params.Verbose >= 2)
    {
        cout << "Adding sample:" << endl;
        Simulator.DisplayState(*sample, cout);
    }
}

int QMDP::GreedyUCB(VNODE* vnode, bool ucb, SIMULATOR *sim, HISTORY *his, double **UCB1) const
{
    vector<int> besta;
    besta.clear();
    double bestq = -Infinity;
    int N = vnode->Value.GetCount();
    double logN = log(N + 1);
    bool hasalpha = sim->HasAlpha();
    //cout<<"Number of actions: "<<Simulator.GetNumActions()<<"\n";

    for (int action = 0; action < sim->GetNumActions(); action++)
    {
      //cout<<"Value of action: "<<action<<"\n";
        double q, alphaq;
        int n, alphan;

        VNODE* qnode = vnode->Child(action);
        q = qnode->Value.GetValue();
        n = qnode->Value.GetCount();

        /*if (Params.UseRave && qnode.AMAF.GetCount() > 0)
        {
            double n2 = qnode.AMAF.GetCount();
            double beta = n2 / (n + n2 + Params.RaveConstant * n * n2);
            q = (1.0 - beta) * q + beta * qnode.AMAF.GetValue();
        }*/

        if (hasalpha && n > 0)
        {
            sim->AlphaValue(qnode, alphaq, alphan);
            q = (n * q + alphan * alphaq) / (n + alphan);
            //cout << "N = " << n << ", alphaN = " << alphan << endl;
            //cout << "Q = " << q << ", alphaQ = " << alphaq << endl;
        }

        if (ucb)
            q += FastUCB(N, n, logN, UCB1);

        if (q >= bestq)
        {
            if (q > bestq)
                besta.clear();
            bestq = q;
            besta.push_back(action);
			}
    	}
		//cout<<"besta size: "<<besta.size()<<"\n";
		//fflush(NULL);
    	assert(!besta.empty());
		return besta[rand()%besta.size()];
    	//return besta[Random(besta.size())];
}


int QMDP::GreedyUCB(VNODE* vnode, bool ucb, SIMULATOR *sim, HISTORY *his) const
{
    static vector<int> besta;
    besta.clear();
    double bestq = -Infinity;
    int N = vnode->Value.GetCount();
    double logN = log(N + 1);
    bool hasalpha = sim->HasAlpha();
    //cout<<"Number of actions: "<<Simulator.GetNumActions()<<"\n";

    for (int action = 0; action < sim->GetNumActions(); action++)
    {
      //cout<<"Value of action: "<<action<<"\n";
        double q, alphaq;
        int n, alphan;

        VNODE* qnode = vnode->Child(action);
        q = qnode->Value.GetValue();
        n = qnode->Value.GetCount();

        /*if (Params.UseRave && qnode.AMAF.GetCount() > 0)
        {
            double n2 = qnode.AMAF.GetCount();
            double beta = n2 / (n + n2 + Params.RaveConstant * n * n2);
            q = (1.0 - beta) * q + beta * qnode.AMAF.GetValue();
        }*/

        if (hasalpha && n > 0)
        {
            sim->AlphaValue(qnode, alphaq, alphan);
            q = (n * q + alphan * alphaq) / (n + alphan);
            //cout << "N = " << n << ", alphaN = " << alphan << endl;
            //cout << "Q = " << q << ", alphaQ = " << alphaq << endl;
        }

        if (ucb)
            q += FastUCB(N, n, logN);

        if (q >= bestq)
        {
            if (q > bestq)
                besta.clear();
            bestq = q;
            besta.push_back(action);
        }
    }

    assert(!besta.empty());
    return besta[Random(besta.size())];
}

void QMDP::UCTSearch(SIMULATOR *sim, HISTORY *his, map<int, pair<int, double> > *maxActionMap, double **UCB1)
{
	 int historyDepth = his->Size();
	 
	 for (int n=0;n<Params.NumSimulations;n++)
	 {
		 STATE* state = Root->Beliefs().CreateSample(*sim);
		 sim->Validate(*state);
	    Status.Phase = SIMULATOR::STATUS::TREE;
	    
		 double totalReward = SimulateV(*state, Root, sim, his, maxActionMap, UCB1);
		
	    sim->FreeState(state);
		 his->Truncate(historyDepth);
	 }
}



double QMDP::SimulateV(STATE& state, VNODE* vnode, SIMULATOR *sim, HISTORY *his, map<int, pair<int, double> > *maxActionMap, double **UCB1)
{
    int action = GreedyUCB(vnode, true, sim, his, UCB1);


    VNODE* qnode = vnode->Child(action);
    double totalReward = SimulateQ(state, qnode, action, sim, his);

	if (maxActionMap->find(action)!=maxActionMap->end())
	{
		maxActionMap->find(action)->second.first += 1;
		maxActionMap->find(action)->second.second += totalReward; 
	}
	else
		maxActionMap->insert(pair<int, pair<int, double> >(action, pair<int, double>(1, totalReward)));

    vnode->Value.Add(totalReward);
	 
    return totalReward;
}

void QMDP::AddRave(VNODE* vnode, double totalReward)
{
    double totalDiscount = 1.0;
    for (int t = TreeDepth; t < History.Size(); ++t)
    {
        VNODE* qnode = vnode->Child(History[t].Action);
        //qnode.AMAF.Add(totalReward, totalDiscount);
        totalDiscount *= Params.RaveDiscount;
    }
}


double QMDP::SimulateQ(STATE& state, VNODE* qnode, int action, SIMULATOR *sim, HISTORY *his)
{
    int observation;
    double immediateReward, delayedReward = 0;

    if (sim->HasAlpha())
        sim->UpdateAlpha(qnode, state);
    bool terminal = sim->Step(state, action, immediateReward);
    his->Add(action);


    if (Params.Verbose >= 3)
    {
        sim->DisplayAction(action, cout);
        sim->DisplayObservation(state, observation, cout);
        sim->DisplayReward(immediateReward, cout);
        sim->DisplayState(state, cout);
    }
	
	SOCIAL_STATE *currSt = new SOCIAL_STATE();
	SOCIAL_STATE& tempcState = static_cast<SOCIAL_STATE&>(state);

	for (int i=0;i<tempcState.netVector.size();i++)
		currSt->netVector.push_back(tempcState.netVector[i]);
	
	VNODE* vnode = qnode;

    if (!terminal)
    {
        if (vnode)
		  {
				pthread_mutex_lock (&mutex);
				vnode->Beliefs().AddSample(currSt);
				pthread_mutex_unlock (&mutex);
            delayedReward = SimulateAfterOneLevel(state, vnode, sim, his);
		  }
    }

    double totalReward = immediateReward + sim->GetDiscount() * delayedReward;
    qnode->Value.Add(totalReward);
    return totalReward;
}


/////need to maintain histories...you are using that everywhere in step function
double QMDP::SimulateAfterOneLevel(STATE& state, VNODE* vnode, SIMULATOR *sim, HISTORY *his)
{
	double reward=0;
	int numSteps=0;
	double currReward=0;
	bool terminal=false;
	int action;
	while(!terminal)
	{
		vector<int> tempLegalActions;
		sim->GenerateLegal(state, *his, tempLegalActions,Status); 
		//action = GreedyUCB(state, true);
		action = tempLegalActions[rand()%(tempLegalActions.size())];
		terminal = sim->Step(state, action, currReward);
		his->Add(action);
		reward+=currReward;
	}

	return reward;
	 
}


void QMDP::AddTransforms(VNODE* root, BELIEF_STATE& beliefs)
{
    int attempts = 0, added = 0;

    // Local transformations of state that are consistent with history
    while (added < Params.NumTransforms && attempts < Params.MaxAttempts)
    {
        STATE* transform = CreateTransform();
        if (transform)
        {
            beliefs.AddSample(transform);
            added++;
        }
        attempts++;
    }

    if (Params.Verbose >= 1)
    {
        cout << "Created " << added << " local transformations out of "
            << attempts << " attempts" << endl;
    }
}

STATE* QMDP::CreateTransform() const
{
    int stepObs;
    double stepReward;

    STATE* state = Root->Beliefs().CreateSample(Simulator);
    Simulator.Step(*state, History.Back().Action, stepReward);
    if (Simulator.LocalMove(*state, History, Status))
        return state;
    Simulator.FreeState(state);
    return 0;
}


double QMDP::UCB[UCB_N][UCB_n];
bool QMDP::InitialisedFastUCB = true;

void QMDP::InitFastUCB(double exploration, double **UCB1)
{
    //cout << "Initialising fast UCB table... ";
    for (int N = 0; N < UCB_N; ++N)
        for (int n = 0; n < UCB_n; ++n)
            if (n == 0)
                UCB1[N][n] = Infinity;
            else
                UCB1[N][n] = exploration * sqrt(log(N + 1) / n);
    //cout << "done" << endl;
    InitialisedFastUCB = true;
}

void QMDP::InitFastUCB(double exploration)
{
    cout << "Initialising fast UCB table... ";
    for (int N = 0; N < UCB_N; ++N)
        for (int n = 0; n < UCB_n; ++n)
            if (n == 0)
                UCB[N][n] = Infinity;
            else
                UCB[N][n] = exploration * sqrt(log(N + 1) / n);
    cout << "done" << endl;
    InitialisedFastUCB = true;
}

inline double QMDP::FastUCB(int N, int n, double logN, double **UCB1) const
{
    if (InitialisedFastUCB && N < UCB_N && n < UCB_n)
        return UCB1[N][n];

    if (n == 0)
        return Infinity;
    else
        return Params.ExplorationConstant * sqrt(logN / n);
}

inline double QMDP::FastUCB(int N, int n, double logN) const
{
    if (InitialisedFastUCB && N < UCB_N && n < UCB_n)
        return UCB[N][n];

    if (n == 0)
        return Infinity;
    else
        return Params.ExplorationConstant * sqrt(logN / n);
}

void QMDP::ClearStatistics()
{
    StatTreeDepth.Clear();
    StatRolloutDepth.Clear();
    StatTotalReward.Clear();
}

void QMDP::DisplayStatistics(ostream& ostr) const
{
    if (Params.Verbose >= 1)
    {
        StatTreeDepth.Print("Tree depth", ostr);
        StatRolloutDepth.Print("Rollout depth", ostr);
        StatTotalReward.Print("Total reward", ostr);
    }

    if (Params.Verbose >= 2)
	 {
        ostr << "Policy after " << Params.NumSimulations << " simulations" << endl;
        DisplayPolicy(6, ostr);
        ostr << "Values after " << Params.NumSimulations << " simulations" << endl;
        DisplayValue(6, ostr);
    }
}

void QMDP::DisplayValue(int depth, ostream& ostr) const
{
    HISTORY history;
    ostr << "QMDP Values:" << endl;
    //Root->DisplayValue(history, depth, ostr);
}

void QMDP::DisplayPolicy(int depth, ostream& ostr) const
{
    HISTORY history;
    ostr << "QMDP Policy:" << endl;
    //Root->DisplayPolicy(history, depth, ostr);
}


