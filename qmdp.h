#ifndef MCTS_H
#define MCTS_H

#include "simulator.h"
#include "node.h"
#include "statistic.h"
#include<map>
#include "Graph.h"
#include<string>
#include<pthread.h>
#include<stdlib.h>

#define NUMTHRDS 200

using namespace std;

class QMDP
{
public:

    struct PARAMS
    {
        PARAMS();

        int Verbose;
        int MaxDepth;
        int NumSimulations;
        int NumStartStates;
        bool UseTransforms;
        int NumTransforms;
        int MaxAttempts;
        int ExpandCount;
        double ExplorationConstant;
        bool UseRave;
        double RaveDiscount;
        double RaveConstant;
        bool DisableTree;
    };

    QMDP(SIMULATOR& simulator, const PARAMS& params, int numberMDP, bool weightedvoting, string graphfile, int diffusionTime, bool option2, double exploration);
    ~QMDP();

	 void *ThreadSimulator(void *b);

    int SelectAction();
    bool Update(int action, double reward);

    void UCTSearch(SIMULATOR *sim, HISTORY *his, map<int, pair<int, double> > *maxActionMap, double **UCB);////new version
    void RolloutSearch();

    double Rollout(STATE& state);

    const BELIEF_STATE& BeliefState() const { return Root->Beliefs(); }
    const HISTORY& GetHistory() const { return History; }
    const SIMULATOR::STATUS& GetStatus() const { return Status; }
    void ClearStatistics();
    void DisplayStatistics(std::ostream& ostr) const;
    void DisplayValue(int depth, std::ostream& ostr) const;
    void DisplayPolicy(int depth, std::ostream& ostr) const;

    static void UnitTest();
    static void InitFastUCB(double exploration);////old version
	void InitFastUCB(double exploration, double **UCB);///new version

private:
	 double MaxReward;
	 int maxAction;
	 int numberMDP;
	 bool weightedvoting, option2;
	 string graphfile;
	 int diffusionTime;
	 map<int, pair<int, double> > finalActionMap;

	 pthread_t callThd[NUMTHRDS];
     pthread_mutex_t mutex;

	 


    SIMULATOR& Simulator;
    int TreeDepth, PeakTreeDepth;
	double explorationConst;
    PARAMS Params;
    VNODE* Root;
    HISTORY History;
    SIMULATOR::STATUS Status;

    STATISTIC StatTreeDepth;
    STATISTIC StatRolloutDepth;
    STATISTIC StatTotalReward;

	int GreedyUCB(VNODE* vnode, bool ucb, SIMULATOR *sim, HISTORY *his) const; ////older version
    int GreedyUCB(VNODE* vnode, bool ucb, SIMULATOR *sim, HISTORY *his, double **UCB) const;
    int SelectRandom() const;
    double SimulateV(STATE& state, VNODE* vnode, SIMULATOR *sim, HISTORY *his, map<int, pair<int, double> > *maxActionMap, double **UCB1);///new version
    double SimulateQ(STATE& state, VNODE* qnode, int action, SIMULATOR *sim, HISTORY *his);
	double SimulateAfterOneLevel(STATE& state, VNODE* vnode, SIMULATOR *sim, HISTORY *his);
    void AddRave(VNODE* vnode, double totalReward);
    VNODE* ExpandNode(const STATE* state);
    void AddSample(VNODE* node, const STATE& state);
    void AddTransforms(VNODE* root, BELIEF_STATE& beliefs);
    STATE* CreateTransform() const;
    void Resample(BELIEF_STATE& beliefs);

    // Fast lookup table for UCB
    static const int UCB_N = 10000, UCB_n = 100;
    static double UCB[UCB_N][UCB_n];
    static bool InitialisedFastUCB;

    double FastUCB(int N, int n, double logN) const;////older version
	 double FastUCB(int N, int n, double logN, double **UCB) const;///new version
};

#endif // MCTS_H
