#include "social.h"
#include<time.h>
#include<math.h>
using namespace std;

int SOCIAL::comb(int n, int k) const
{
	if (n<k)
		return 0;
	if (n==k)
		return 1;
	
    int i,j;
    int a[n][n];
    for(i=0;i<=n;i++)
    {
       for(j=0;j<=(i>k?k:i);j++)
       {
            if(j==0||j==i)  //if j==0 or j==i , coefficient =1 as nC0=1 and nCn=1 where C= combination
            {
                a[i][j]=1;
            }
            else
                a[i][j]=a[i-1][j-1]+a[i-1][j];  // nCk=(n-1)C(k-1) + (n-1)C(k)
        }
	 }
    return a[n][k];// return value of nCk
}


SOCIAL::SOCIAL(string graphFile, int Time, int k, int numActions, Graph *gra, map<int, vector<int> > *map, double discountVal, bool isSim)
{
	isSimulator = isSim;
	graph = gra;
	actionMap = map;
	K=k;
	
   numLegalActions = numActions;
   Discount = discountVal;
   RewardRange = 0.1;
   srand(time(NULL));
   NumActions = actionMap->size();
	cout<<"Number of actions in constructor: "<<GetNumActions()<<"\n";
}

STATE* SOCIAL::CreateStartState() const
{
	SOCIAL_STATE* bsstate = MemoryPool.Allocate();

	/////initially all nodes are uninfluenced
	for (int i=0;i<graph->numNodes;i++)
		bsstate->netVector.push_back(0);
	
	return bsstate;
}

void SOCIAL::FreeState(STATE *state) const
{
	SOCIAL_STATE* bsstate = safe_cast<SOCIAL_STATE*>(state);
   MemoryPool.Free(bsstate);
}

STATE* SOCIAL::Copy(const STATE& state) const
{
	 assert(state.IsAllocated());
    const SOCIAL_STATE& oldstate = safe_cast<const SOCIAL_STATE&>(state);
    SOCIAL_STATE* newstate = MemoryPool.Allocate();
    *newstate = oldstate;
    return newstate;
}

void SOCIAL::Validate(const STATE& state) const
{
	const SOCIAL_STATE& bsstate = safe_cast<const SOCIAL_STATE&>(state);
	for (int i=0;i<bsstate.netVector.size();i++)
	{
		if (bsstate.netVector[i]==1||bsstate.netVector[i]==0)
			continue;
		else
			assert(false);
	}
}

void SOCIAL::DisplayBeliefs(const BELIEF_STATE& beliefState,
    ostream& ostr) const
{
	cout<<"Belief: \n";
	for (int i=0;i<beliefState.GetNumSamples();i++)
	{
		cout<<"State: \n";
		const SOCIAL_STATE *bsstate = safe_cast<const SOCIAL_STATE*>(beliefState.GetSample(i));
		//const SOCIAL_STATE& bsstate = safe_cast<SOCIAL_STATE&>(beliefState.GetSample(i));
		 for (int i=0;i<bsstate->netVector.size();i++)
      	cout<<bsstate->netVector[i]<<",";
   	 cout<<"\n";
	}
}

void SOCIAL::DisplayState(const STATE& state, ostream& ostr) const
{
	cout<<"State: \n";
	const SOCIAL_STATE& bsstate = safe_cast<const SOCIAL_STATE&>(state);
	for (int i=0;i<bsstate.netVector.size();i++)
		cout<<bsstate.netVector[i]<<",";
	cout<<"\n";
}


void SOCIAL::DisplayAction(int action, ostream& ostr) const
{
	cout<<"Action: \n";

	vector<int> observ = actionMap->find(action)->second;
   for (int i=0;i<observ.size();i++)
      cout<<observ[i]<<",";
   cout<<"\n";
}

bool SOCIAL::LocalMove(STATE& state, const HISTORY& history, const STATUS& status) const
{
	SOCIAL_STATE& bsstate = safe_cast<SOCIAL_STATE&>(state);
	vector<int> unionState;
	for (int i=0;i<bsstate.netVector.size();i++)
		unionState.push_back(0);
	
	for (int t=0;t<history.Size();t++)
	{
		 int action = history[t].Action;
		 vector<int> actionVec = actionMap->find(action)->second;
		
		for (int j=0;j<graph->numNodes;j++)
		{
			if (actionVec[j]==1)
				unionState[j]=1;
		}
	}

	for (int i=0;i<bsstate.netVector.size();i++)
	{
		if (unionState[i]==0)
			bsstate.netVector[i]=rand()%2;
	}
		
	return true;
}

int SOCIAL::SelectRandom(const STATE& state, const HISTORY& history) const
{
	const SOCIAL_STATE& bsstate = safe_cast<const SOCIAL_STATE&>(state);
	
	vector<int> unionState;
	for (int i=0;i<graph->numNodes;i++)
		unionState.push_back(0);
	
	for (int t=0;t<history.Size();t++)
	{
		int action = history[t].Action;
		vector<int> actionVec = actionMap->find(action)->second;
      	
		for (int j=0;j<graph->numNodes;j++)
      	{
         	if (actionVec[j]==1)
            unionState[j]=1;
      	}
	}
	
	/////generate random legal action...make sure that action picks nodes which have not been picked before
	vector<int> zeroIndices;
	vector<int> oneIndices;
	int numZeros=0;
	for (int i=0;i<unionState.size();i++)
	{
		if (unionState[i]==0)
		{
			numZeros++;
			zeroIndices.push_back(i);
		}
		else
		{
			oneIndices.push_back(i);
		}
	}

	int numChosen=0;
	
	vector<int> chosenAction;
	for (int i=0;i<graph->numNodes;i++)
		chosenAction.push_back(0);
	
	//cout<<"zeroIndices.size(): "<<zeroIndices.size()<<"\n";
	
	while(zeroIndices.size()>0&&numChosen<K)
	{
		int rando = rand()%(zeroIndices.size());
		chosenAction[zeroIndices[rando]]=1;
		zeroIndices.erase(zeroIndices.begin()+rando);
		numChosen++;
	}
	
	///only if you have less than K unpicked nodes...now need to pick from already picked nodes
	while(numChosen<K)
	{
		//cout<<"Hello\n";
		int rando = rand()%(oneIndices.size());
      chosenAction[oneIndices[rando]]=1;
      oneIndices.erase(oneIndices.begin()+rando);
		numChosen++;
	}
	
	int resultAction=0;
	int indexaw=0;
	for (map<int, vector<int> >::const_iterator it=actionMap->begin();it!=actionMap->end();it++,indexaw++)
	{
		vector<int> currAcc = it->second;
		bool same=true;
		for (int f=0;f<currAcc.size();f++)
		{
			if (currAcc[f]!=chosenAction[f])
			{
				same=false;
				break;
			}	
		}
		if (same)
		{
			resultAction=indexaw;
			break;
		}
	}
	
	return resultAction;
}

void SOCIAL::GenerateLegal(const STATE& state, const HISTORY& history, vector<int>& legal) const
{
	const SOCIAL_STATE& bsstate = safe_cast<const SOCIAL_STATE&>(state);
   vector<int> unionState;
   for (int i=0;i<graph->numNodes;i++)
      unionState.push_back(0);

   for (int t=0;t<history.Size();t++)
   {
      	int action = history[t].Action;
		vector<int> actionVec = actionMap->find(action)->second;

      	for (int j=0;j<graph->numNodes;j++)
      	{
         	if (actionVec[j]==1)
           		unionState[j]=1;
      	}
   }

	legal.clear();
	vector<int> zeroIndices;
   vector<int> oneIndices;

	
	int m=0;
	while(m<numLegalActions)///loop start
	{
	zeroIndices.clear();
	oneIndices.clear();
   int numZeros=0;
   for (int i=0;i<unionState.size();i++)
   {
      if (unionState[i]==0)
      {
         numZeros++;
         zeroIndices.push_back(i);
      }
      else
      {
         oneIndices.push_back(i);
      }
   }

   int numChosen=0;

   vector<int> chosenAction;
   for (int i=0;i<graph->numNodes;i++)
      chosenAction.push_back(0);

   while(zeroIndices.size()>0&&numChosen<K)
   {
      int rando = rand()%(zeroIndices.size());
      chosenAction[zeroIndices[rando]]=1;
      zeroIndices.erase(zeroIndices.begin()+rando);
      numChosen++;
   }
   
   ///only if you have less than K unpicked nodes...now need to pick from already picked nodes
   while(numChosen<K)
   {
      int rando = rand()%(oneIndices.size());
      chosenAction[oneIndices[rando]]=1;
      oneIndices.erase(oneIndices.begin()+rando);
      numChosen++;
   }

	int resultAction=0;
	int indexaw=0;
   	for (map<int, vector<int> >::const_iterator it=actionMap->begin();it!=actionMap->end();it++,indexaw++)
   	{
      vector<int> currAcc = it->second;
      bool same=true;
      for (int f=0;f<currAcc.size();f++)
      {
         if (currAcc[f]!=chosenAction[f])
         {
            same=false;
            break;
         }  
      }
      if (same)
      {
         resultAction=indexaw;
         break;
      }
   }

		legal.push_back(resultAction);
		m++;

	}///loop end
	
		
}

bool SOCIAL::Step(STATE& state, int action, double& reward) const
{
   SOCIAL_STATE& bsstate = safe_cast<SOCIAL_STATE&>(state);
  
   int initialVal=0;
   for (int i=0;i<graph->numNodes;i++)
      if (bsstate.netVector[i]==1)
         initialVal++;

   vector<int> actionVec = actionMap->find(action)->second;
  
   /*if (isSimulator)
   {
   for (int i=0;i<graph->numNodes;i++)
   {
      if (actionVec[i]==1&&bsstate.netVector[i]==1)
      {
         reward=0;
		 //return false;
         //assert(false);
      }
   }
   }*/
  
   double **newadjMatrix = new double*[graph->numNodes];

   for (int i=0;i<graph->numNodes;i++)
   {
      newadjMatrix[i] = new double[graph->numNodes];
      for (int j=0;j<graph->numNodes;j++)
         newadjMatrix[i][j]=0;
   }

   set<int> *currentObservation = new set<int>();
   int indexa=0;
   for (map<int, map<int, pair<int, int> > >::iterator it= graph->adjList.begin();it!=graph->adjList.end();it++, indexa++)
   {
      if (actionVec[indexa]==1)
      {
          map<int, pair<int, int> > currList = it->second;
          for (map<int, pair<int, int> >::iterator listIt = currList.begin(); listIt!=currList.end();listIt++)
          {
             int endVertex=0;
				 int count=0;
             for (map<int, map<int, pair<int, int> > >::iterator itee=graph->adjList.begin();itee!=graph->adjList.end();itee++,count++)
             {
                if (itee->first==listIt->first)
                {
                     endVertex = count;
                     break;
                }
             }
              ////end vertex found

             if ((listIt->second).second==0)///uncertain edge
             {
					 pair<int, int> tum = graph->uncertainEdgeList.find((listIt->second).first)->second;
					 //double rara = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
					 //int randum;
					 //if (rara<graph->existenceProb)
					//	randum=1;
					 //else
					////	randum=0; 
					 int randum = rand()%2;
					 newadjMatrix[indexa][endVertex] = randum;
					 if (randum==1)
					 {
						listIt->second.second=1;/////no longer an uncertain edge
					 	cout<<"Edge between "<<it->first<<" and "<<listIt->first<<" exists.\n";
						cout<<"Edge between "<<tum.first<<" and "<<tum.second<<" exists.\n";
					 }
					 else if (randum==0)////remove edge from uncertain list
					 {
						listIt->second.second = -5; /////represents that this was an uncertain edge in the past...upon observation...it was found to be non existent
						cout<<"Edge between "<<it->first<<" and "<<listIt->first<<" does not exist.\n";
						cout<<"Edge between "<<tum.first<<" and "<<tum.second<<" does not exist.\n";
					 }
					
 					 //graph->uncertainEdgeList.erase(listIt->second.first);
                currentObservation->insert((listIt->second).first);///these are the indexes of the uncertainEdgeList
             }
             else if (listIt->second.second!=-5)////certain edge
             {
               newadjMatrix[indexa][endVertex] = 1;
             }
          }
      }
      else if (bsstate.netVector[indexa]==1)
      {
         //cout<<"HANNNAD\n";
          map<int, pair<int, int> > currList = it->second;
          for (map<int, pair<int, int> >::iterator listIt = currList.begin(); listIt!=currList.end();listIt++)
          {
             int endVertex=0;
             int count=0;
             for (map<int, map<int, pair<int, int> > >::iterator itee=graph->adjList.begin();itee!=graph->adjList.end();itee++,count++)
             {
                if (itee->first==listIt->first)
                {
                     endVertex = count;
                     break;
                }
             }
              ////end vertex found
            if ((listIt->second).second==0)///uncertain edge
             {
					  newadjMatrix[indexa][endVertex] = 0.5;
             }
             else if ((listIt->second).second==-5)////uncertain edge which was non existent
             {
               newadjMatrix[indexa][endVertex] = 0;
             }
				 else if ((listIt->second).second==1)///certain edge
				{
				  newadjMatrix[indexa][endVertex] = 1;
				}
         }
      }
      else
         continue;
   }

            double *diffusionCentrality = new double[graph->numNodes];
            double *columnDiffusionCentrality = new double[graph->numNodes];
            double** temp = new double*[graph->numNodes];
            for(int i = 0; i < graph->numNodes; ++i)
               temp[i] = new double[graph->numNodes];

            double** temp2 = new double*[graph->numNodes];
            for(int i = 0; i < graph->numNodes; ++i)
               temp2[i] = new double[graph->numNodes];

            ///this variable is just to make sure that both colDiff and diffusionCentr can be populated in the same set of loops
            int altIndex=0;
            for (int i=0;i<graph->numNodes;i++)
            {
                diffusionCentrality[i]=0;
                columnDiffusionCentrality[i]=0;
                for (int j=0;j<graph->numNodes;j++)
                {
                    temp[i][j]=newadjMatrix[i][j];
                    temp2[i][j]=newadjMatrix[i][j];
                    diffusionCentrality[i] += 0.5*temp[i][j];
                    columnDiffusionCentrality[i] += 0.5*newadjMatrix[j][altIndex];
                }
                altIndex++;
            }


            double prob = 0.25;
            for (int i=2;i<=graph->T;i++, prob = prob*0.5)
            {
                for (int j=0;j<graph->numNodes;j++)
					  {
                    for (int k=0;k<graph->numNodes;k++)
                    {
                        double sum=0;
                        for (int h=0;h<graph->numNodes;h++)
                            sum+=temp2[j][h]*newadjMatrix[h][k];

                        temp[j][k]=sum;
                        diffusionCentrality[j] += prob*temp[j][k];
                    }
                }

               for (int j=0;j<graph->numNodes;j++)
               {
                  for (int k=0;k<graph->numNodes;k++)
                  {
                     columnDiffusionCentrality[j] += prob*temp[k][j];
                  }
               }


                for (int j=0;j<graph->numNodes;j++)
                    for (int k=0;k<graph->numNodes;k++)
                        temp2[j][k]=temp[j][k];
            }

            /////delete allocated memory
            for(int i = 0; i < graph->numNodes; ++i) {
               delete [] newadjMatrix[i];
               delete [] temp[i];
               delete [] temp2[i];
            }



    double total=0;
   map<int, double> sampleProb;
   for (int i=0;i<graph->numNodes;i++)
   {
      if (bsstate.netVector[i]==1||actionVec[i]==1)
         bsstate.netVector[i]=1;
      else
      {
         sampleProb.insert(pair<int, double>(i, columnDiffusionCentrality[i]));
         total+=columnDiffusionCentrality[i];
      }
   }

   for (int i=0;i<graph->numNodes;i++)
   {
      if (bsstate.netVector[i]!=1)
      {
         double prob= (sampleProb.find(i)->second)/total;
			 double r = static_cast <double> (rand()) / static_cast <double> (RAND_MAX);
         if (r<prob)
            bsstate.netVector[i]=1;
         else
            bsstate.netVector[i]=0;
      }
   }

   /////next come up with the rewards

   reward=0;
   for (int i=0;i<graph->numNodes;i++)
      reward+=bsstate.netVector[i];

   reward = reward-initialVal;

   ///check is state is terminal..return true if this is the case
   bool flag=true;
   for (int i=0;i<graph->numNodes;i++)
   {
      if (bsstate.netVector[i]!=1)
      {
         flag=false;
         break;
      }
   }
  
   delete diffusionCentrality;
   delete columnDiffusionCentrality;

   return flag;

}


