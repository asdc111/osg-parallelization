#include<fstream>
#include<sstream>
#include<iostream>
#include<map>
#include<string>

using namespace std;

int main()
{
	map<int, double> voteMap;
	ifstream temp1;
	string fil("voting");
	int count=1;
	temp1.open(fil.append(to_string(count)));
	
	int action, vote;
	while(temp1.is_open())
	{
		temp1>>action>>vote;
		if (voteMap.find(action)!=voteMap.end())
        	voteMap.find(action)->second += vote;
   	 	else
        	voteMap.insert(pair<int, double>(action, vote));
	
		temp1.close();
		temp1.clear();
		count++;
		fil.assign("voting");
		temp1.open(fil.append(to_string(count)));
	}


	
	int maxVoter=-1;
    int maxAction = -1;
    for (map<int, double>::iterator it = voteMap.begin();it!=voteMap.end();it++)
    {
        if (it->second>maxVoter)
        {
            maxVoter = it->second;
            maxAction = it->first;
        }
    }

	ofstream final_out;
	final_out.open("final_best_action");
	if (final_out.is_open())
	{
		final_out<<maxAction<<"\n";
	}
	final_out.close();

	return 0;
}
